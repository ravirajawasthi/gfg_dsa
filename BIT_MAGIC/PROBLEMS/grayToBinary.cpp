/*
Given N in Gray code equivalent. Find its binary equivalent.

Input:
The first line contains an integer T, number of test cases. For each test cases, there is an integer N denoting the number in gray equivalent.

Output:
For each test case, in a new line, the output is the decimal equivalent number N of binary form.

User Task:
Your task is to complete the function grayToBinary() which returns decimal of the binary equivalent of the given gray number.

Constraints:
1 <= T <= 100
0 <= N <= 108

Example:
Input:
3
4
15
0

Output:
7
10
0

Explanation:
Testcase1. 4 is represented as gray 100 and its binary equivalent is 111 whose decimal equivalent is 7.
Testcase2. 15 is represented as gray 1111 and its binary equivalent is 1010 i.e. 10 in decimal.
Testcase3: Zero is zero in gray and in binary and decimal.
*/

// { Driver Code Starts
//Initial Template for C++

#include <bits/stdc++.h>
using namespace std;

// } Driver Code Ends
//User function Template for C++

// function to conver given N Gray to Binary
int grayToBinary(int n)
{
    if (n == 0) return 0;
    vector<int> binary;
    while (n > 0)
    {
        if (n % 2 == 0)
            binary.push_back(0);
        else
            binary.push_back(1);
        n = n / 2;
    }
    vector<int> res(binary.size());
    res.back() = binary.back();
    for (int i = binary.size() - 2; i > -1; i--)
    {
        res[i] = binary[i] ^ res[i + 1];
    }
    int fN = 0, k = 0;
    for (int x : res)
    {
        fN = fN + (x << k++);
    }
    return fN;
}

// { Driver Code Starts.

// Driver code
int main()
{
    int t, n;
    cin >> t;
    while (t--)
    {
        cin >> n;
        cout << grayToBinary(n) << endl;
    }
}
// } Driver Code Ends