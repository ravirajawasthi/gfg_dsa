bool isSparse(int n)
{
    if (n & (n << 1) != 0)
        return 0;
    return 1;
}
