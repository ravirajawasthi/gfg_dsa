#include <bits/stdc++.h>
using namespace std;

int naiveSolution(int);
int BKAlgo(int);

int main()
{
    int t;
    cout << "||=================Count active bits=================||" << endl;
    cout << "Only for +ve numbers" << endl;
    cout << "Test cases? : ";
    cin >> t;
    int n;
    for (int i = 0; i < t; i++)
    {
        cout << "Enter n : ";
        cin >> n;
        cout << "Active bits are : " << naiveSolution(n) << " " << BKAlgo(n) << endl;
    }
    return 0;
}

int naiveSolution(int n)
{
    int res = 0;
    while (n > 0)
    {
        if (n % 2 != 0)
            res++;
        n = n / 2;
    }
    return res;
}

//=============================Brian and Kerningham Algorithm======================================
int BKAlgo(int n)
{
    int count = 0;
    while (n > 0)
    {
        n = (n & (n - 1));
        count++;
    }
    return count;
}