#include <bits/stdc++.h>
using namespace std;

bool kBitActive(int, int);

int main()
{
    int t;
    cout << "||=================Check if kth bit is active=================||" << endl;
    cout << "Only for +ve numbers" << endl;
    cout << "Test cases? : ";
    cin >> t;
    int n, k;
    bool res;
    for (int i = 0; i < t; i++)
    {
        cout << "Enter n and k separated by space : ";
        cin >> n >> k;
        res = kBitActive(n, k);
        res ? cout << "Active!" : cout << "Not Active";
        cout << endl;
    }
    return 0;
}

bool kBitActive(int n, int k)
{
    int a = (1 << (k - 1));
    if ((a & n) > 0)
        return true;
    else
        return false;
}