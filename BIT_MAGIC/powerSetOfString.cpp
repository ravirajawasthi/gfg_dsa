#include <bits/stdc++.h>
using namespace std;

vector<string> powerSetGenerator(string);

int main()
{
    string s;
    cin >> s;
    vector<string> res = powerSetGenerator(s);
    for (string i : res)
        cout << i << " ";
    cout << endl;
    return 0;
}

vector<string> powerSetGenerator(string s)
{

    vector<string> res;

    int stringSize = s.size();
    string temp;
    int t, i;
    for (int n = 0; n < (pow(2, stringSize)); n++)
    {
        t = n;
        temp = "";
        i = 0;
        while (t > 0)
        {
            int bin = (t % 2);
            if (bin == 1)
                temp.push_back(s[i]);
            i++;
            t = t / 2;
        }

        res.push_back(temp);
    }
    return res;
}