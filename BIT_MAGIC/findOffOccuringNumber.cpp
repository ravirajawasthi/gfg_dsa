//Find odd occuring number in a array

#include <bits/stdc++.h>
using namespace std;

int findOddOneOut(vector<int> &);

int main()
{
    int t;
    cout << "||=================find one number that is occuring odd times=================||" << endl;
    cout << "Only 1 number must occur odd times" << endl;
    cout << "Test cases? : ";
    cin >> t;
    int n;
    for (int i = 0; i < t; i++)
    {
        cout << "Number of elements in array? : ";
        cin >> n;
        vector<int> array(n);
        for (int i = 0; i < n; i++)
            cin >> array[i];
        cout << "Odd number out is : " << findOddOneOut(array) << endl;
    }
    return 0;
}

//=============================Brian and Kerningham Algorithm======================================
int findOddOneOut(vector<int> &array)
{
    int res = 0;
    for (int i = 0; i < array.size(); i++)
    {
        res = res ^ array[i];
    }
    return res;
}