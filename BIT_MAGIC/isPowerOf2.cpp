#include <bits/stdc++.h>
using namespace std;

bool BKAlgo(int);

int main()
{
    int t;
    cout << "||=================Is Power of 2=================||" << endl;
    cout << "Only for +ve numbers" << endl;
    cout << "Test cases? : ";
    cin >> t;
    int n;
    for (int i = 0; i < t; i++)
    {
        cout << "Enter n : ";
        cin >> n;
        BKAlgo(n) ? cout << "Yes, it is power of 2" : cout << "No, not a power of 2";
        cout << endl;
    }
    return 0;
}

//=============================Brian and Kerningham Algorithm======================================
bool BKAlgo(int n)
{
    if (n == 0)
        return false;
    else
        return ((n & (n - 1)) == 0);
}