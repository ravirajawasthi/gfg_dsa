//Find odd occuring number in a array

#include <bits/stdc++.h>
using namespace std;

void findOddTwoOut(vector<int> &);

int main()
{
    int t;
    cout << "||=================find 2 number that is occuring odd times=================||" << endl;
    cout << "Only 2 number must occur odd times" << endl;
    cout << "Test cases? : ";
    cin >> t;
    int n;
    for (int i = 0; i < t; i++)
    {
        cout << "Number of elements in array? : ";
        cin >> n;
        vector<int> array(n);
        for (int i = 0; i < n; i++)
            cin >> array[i];
        findOddTwoOut(array);
        cout << endl;
    }
    return 0;
}

//=============================Brian and Kerningham Algorithm======================================
void findOddTwoOut(vector<int> &array)
{
    // XOR of all numbers in the list
    int res = 0;
    for (int i = 0; i < array.size(); i++)
    {
        res = res ^ array[i];
    }

    //Get the last set bit from XOR Result
    // -1 is to remove the right most set bit and ~ to invert it all
    int lastSetBit = res & (~(res - 1));

    //2 groups
    int res1 = 0, res2 = 0;
    for (int x : array)
    {
        if ((x & lastSetBit) == 0)
        {
            res1 = res1 ^ x;
        }
        else
        {
            res2 = res2 ^ x;
        }
    }
    cout << res1 << " " << res2 << " ";
}
