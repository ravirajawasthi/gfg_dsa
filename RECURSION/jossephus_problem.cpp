#include <bits/stdc++.h>
using namespace std;

unsigned int josephus_problem(unsigned int n, unsigned int k);

int main()
{
    cout << "Josephus Problem" << endl;
    cout << "n and k" << endl;

    int n, k;

    cin >> n >> k;

    cout << "Alive person is " << josephus_problem(n, k) << endl;

    return 0;
}

unsigned int josephus_problem(unsigned int n, unsigned int k)
{
    //vector that contains alive people
    vector<int> participants(n);
    //Everyone is alive in starting
    int alive = n;
    int weapon_holder = 0;
    for (int i = 0; i < n; i++)
    {
        participants[i] = i;
    }
    while (alive > 1)
    {
        int person_to_kill = (weapon_holder + k - 1) % alive;

        //Remove the person from vector
        participants.erase(participants.begin() + person_to_kill);
        //Change the weapon_holder to next person
        weapon_holder = (person_to_kill) % n;
        //Decrement count of perosn alive
        alive -= 1;
    }

    return participants[0] + 1;
}