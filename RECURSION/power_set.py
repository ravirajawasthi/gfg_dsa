# User function Template for python3
import sys
import io
import atexit


def powerSetRecursion(s, N):
    if N == pow(2, len(s)):
        return []
    result = ''
    b = bin(N)[2:]
    b = ('0' * (len(s) - len(b))) + b
    for i in range(len(b)):
        if b[i] == '0':
            result += ''
        else:
            result += s[i]
    return [result] + powerSetRecursion(s, N+1)


def powerSet(s, N=0):
    '''
    :param s: given string s
    :return: list containing power set of s.
    '''
    words = []
    while N != pow(2, len(s)):
        b = bin(N)[2:]
        b = ('0' * (len(s) - len(b))) + b
        result = ''
        for i in range(len(b)):
            if b[i] == '0':
                result += ''
            else:
                result += s[i]
        N += 1
        words.append(result)
    return words


if __name__ == '__main__':
    test_cases = int(input())
    for cases in range(test_cases):
        s = str(input())
        ans = powerSet(s)
        ans.sort()
        print(*ans)
# } Driver Code Ends
