# User function Template for python3


# Complete this function

import math
dictionary = {
    2: ['A', 'B', 'C'],
    3: ['D', 'E', 'F'],
    4: ['G', 'H', 'I'],
    5: ['J', 'K', 'L'],
    6: ['M', 'N', 'O'],
    7: ['P', 'Q', 'R', 'S'],
    8: ['T', 'U', 'V'],
    9: ['W', 'X', 'Y', 'Z'],
    0: [''],
    1: ['']
}

words = []


def possibleWords(a, N):
    for character in dictionary[a[0]]:
        (possibleWordsRecursion(a[1:],  ''+character.lower(), N-1))
    return words


def possibleWordsRecursion(a, prefix, N):
    if N == 0:
        words.append(prefix)
        return

    for character in dictionary[a[0]]:
        possibleWordsRecursion(a[1:],  prefix+character.lower(), N-1)


def main():

    T = int(input())

    while(T > 0):

        N = int(input())
        a = [int(x) for x in input().strip().split()]

        res = possibleWords(a,  N)
        for i in range(len(res)):
            print(res[i], end=" ")

        print()

        T -= 1


if __name__ == "__main__":
    main()
# } Driver Code Ends
